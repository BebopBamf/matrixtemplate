cmake_minimum_required(VERSION 3.15)

project(Matrix
    DESCRIPTION "A simple matrix library"
    LANGUAGES CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

SET(MATRIX_VERSION_MAJOR "0")
SET(MATRIX_VERSION_MINOR "1")
SET(MATRIX_VERSION_PATCH "0")
SET(MATRIX_VERSION "${VULKANOECS_VERSION_MAJOR}.${VULKANOECS_VERSION_MINOR}.${VULKANOECS_VERSION_PATCH}")

add_executable(matrix main.cpp matrix.cpp)

target_compile_features(matrix PUBLIC cxx_std_17)
set_target_properties(matrix PROPERTIES CXX_EXTENSIONS OFF)
